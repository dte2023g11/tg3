from google.cloud import bigtable
from google.oauth2 import service_account
import random

# Ruta al archivo JSON de las credenciales 
credentials_path = 'C:/Users/Usuario/Downloads/tg3-2023-c8391f35d420.json'

# Configuración de las credenciales de autenticación
credentials = service_account.Credentials.from_service_account_file(credentials_path)

# Creamos una instancia de cliente de Bigtable con las credenciales
client = bigtable.Client(credentials=credentials, project='tg3-2023', admin=True)
instance = client.instance('tg32023')

# Conexión a la tabla
table = instance.table('tg3_tabla1')

# Insertar 10,000 filas en la tabla (la primera columna el id del alumno (de 1 a 10000) 
# y la segunda columna las notas que han sacado este curso (enteros aleatorios entre 0 y 10)
for i in range(1, 10001):
    row_key = f'alumno-{i}'
    row = table.row(row_key)
    nota = random.randint(0, 10)
    row.set_cell('column-family', 'nota', str(nota))
    table.mutate_rows([row])

print('Se han introducido con éxito los 10000 datos.')

