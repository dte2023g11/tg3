import boto3
import random

#Conexion con DynamoDB
dynamodb = boto3.resource("dynamodb", region_name="eu-north-1")

#Referencia a la tabla
tabla_alumnos = dynamodb.Table("tg3_tabla1")

for i in range(10000):
    alumno_id = i + 1
    nota = random.randint(0, 100)

    # Crea un nuevo alumno
    alumno = {
        "AlumnosID": alumno_id,
        "Nota": nota
    }

    # Agrega el alumno a la tabla
    tabla_alumnos.put_item(Item=alumno)

print("Se han introducido los 10.000 datos con éxito.")